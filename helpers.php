<?php

/**
 * Helpers files specific to the Synergy Platform
 *
 * Part of the Synergy package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Synergy
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/Synergy
 */

if ( ! function_exists('platform')) {

	/**
	 * Returns instance of Synergy class
	 *
	 * @return \Synergy\Platform\Platform
	 */
	function platform()
	{
		return app('platform');
	}
}