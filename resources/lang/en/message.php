<?php
/**
 * Part of the Synergy package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Synergy
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/Synergy
 */

return [

	'exception' => [
		'no_database_connection' => 'Oops, we could not establish a connection to the database.',
        'ineligible' => 'Please install the Synergy Platform from a trusted source to continue.'
	]
];
