<?php

/**
 * Part of the Synergy package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Synergy
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/Synergy
 */

return [

	/*
    |--------------------------------------------------------------------------
    | Synergy Version
    |--------------------------------------------------------------------------
    |
    | Current configured version of the Synergy Platform. Must be compatible
	| with PHP's version_compare function.
    |
    */
	'version' => false
];