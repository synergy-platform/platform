<?php

namespace Synergy\Platform\Providers;

use Synergy\Support\ServiceProvider;
use Synergy\Platform\Platform;

/**
 * Service Provider for integrating into Laravel
 *
 * Part of the Synergy package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Synergy
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/platform
 */

class PlatformServiceProvider extends ServiceProvider
{

	/**
	 * Boots the Service Provider's resources and services
	 */
	public function boot()
	{
		$this->bootResources();

		$this->app['platform']->boot();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerRespectValidatorPackage();
		$this->registerPlatform();
	}


	/**
	 * Register the Synergy class with the IoC and
	 * aliases the Synergy Facade
	 */
	public function registerPlatform()
	{
		$this->app->singleton('platform', function ($app) {
			return new Platform($app, $app['modules']);
		});

		$this->app->alias('Platform', 'Synergy\Platform\Facades\Synergy');
	}


	/**
	 * Registers the Respect Validator package.
	 */
	protected function registerRespectValidatorPackage()
	{
		$this->app->register('KennedyTedesco\Validation\ValidationServiceProvider');
	}

	/**
	 * Boots the packages resources
	 */
	public function bootResources()
	{
		$this->publishes([
			__DIR__ . '/../../config/config.php' => config_path('platform.php'),
		], 'config');

		$this->loadTranslationsFrom(__DIR__ .'/../../resources/lang/en', 'platform');
	}

	/**
	 * {@inheritDoc}
	 */
	public function provides()
	{
		return [
			'platform'
		];
	}

}