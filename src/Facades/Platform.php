<?php

namespace Synergy\Platform\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Synergy facade for Laravel integration
 *
 * Part of the Synergy package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Synergy
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/synergy
 */

class Platform extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'platform'; }

}